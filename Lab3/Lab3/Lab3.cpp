﻿//#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>

struct Node
{
	Node* next;
	Node* prev;
	char* str;
};

Node* head = nullptr;

Node* get_first() {
	return head;
}

Node* get_last() {
	Node* temp = get_first();

	while (temp != nullptr && temp->next != nullptr) {
		temp = temp->next;
	}

	return temp;
}

void push_back(char* str) {
	Node* temp = get_last();
	Node* new_item = new Node();
	new_item->next = nullptr;
	new_item->prev = temp;
	if (temp != nullptr) {
		temp->next = new_item;
	}
	else {
		head = new_item;
	}
	new_item->str = str;
}

void push_front(char* str) {
	Node* temp = get_first();
	Node* new_item = new Node();
	new_item->next = temp;
	new_item->prev = nullptr;
	if (temp != nullptr) {
		temp->prev = new_item;
	}
	head = new_item;
	new_item->str = str;
}

bool is_empty() {
	return head == nullptr;
}

Node* get_item(int index) {
	if (is_empty()) {
		return nullptr;
	}

	int i = 0;
	Node* cur = head;
	for (i = 0; i < index && cur->next != nullptr; ++i) {
		cur = cur->next;
	}

	if (i != index) {
		return nullptr;
	}

	return cur;
}

void remove(int index) {
	Node* cur = get_item(index);
	if (cur == nullptr) {
		printf("No such item.\n");
		return;
	}

	Node* next = cur->next;
	Node* prev = cur->prev;

	if (prev != nullptr) {
		prev->next = next;
	}

	if (next != nullptr) {
		next->prev = prev;
	}

	if (cur == head) {
		head = cur->next;
	}

	delete cur;
}

void clear() {
	while (head != nullptr) {
		remove(0);
	}
}

void output() {
	Node* cur = head;
	while (cur != nullptr) {
		printf("%s\n", cur->str);
		cur = cur->next;
	}
}

int main()
{
	int index;
	int variant = 0;
	do {
		printf("Available commands:\n\n1. Push string to the end\n2. Push string to the beginning\n3. Remove string by index\n4. Clear list\n5. Show list\n6. Exit\n");
		char* str = new char[256];
		scanf("%d", &variant);
		switch (variant)
		{
		case 1:
			printf("Input string:\n");
			scanf("%s", str);
			push_back(str);
			system("cls");
			break;
		case 2:
			printf("Input string:\n");
			scanf("%s", str);
			push_front(str);
			system("cls");
			break;
		case 3:
			printf("Input index:\n");
			scanf("%d", &index);
			remove(index);
			system("cls");
			break;
		case 4:
			clear();
			system("cls");
			break;
		case 5:
			output();
			system("pause");
			system("cls");
			break;
		case 6:
			system("cls");
			return(0);
		default:
			break;
		}
	} while (variant != 6);

	return 0;
}