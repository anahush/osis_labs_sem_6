// Lab8.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Lab8.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
void				InitControls(HWND);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_LAB8, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB8));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB8));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 820, 420, NULL, NULL, hInstance, NULL);

   InitControls(hWnd);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

#define BUTTON_DRAW_ID 1
#define BUTTON_CLEAR_ID 2

void InitControls(HWND hWnd) {
	HWND buttonAdd = CreateWindow(L"BUTTON", NULL, WS_VISIBLE | WS_CHILD | BS_OWNERDRAW | BS_PUSHBUTTON,
		300, 320, 60, 60, hWnd, (HMENU)BUTTON_DRAW_ID, NULL, NULL);
	HWND buttonClear = CreateWindow(L"BUTTON", NULL, WS_VISIBLE | WS_CHILD | BS_OWNERDRAW | BS_PUSHBUTTON,
		370, 320, 60, 60, hWnd, (HMENU)BUTTON_CLEAR_ID, NULL, NULL);
}

bool pictureVisible = false;

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent, i;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case BUTTON_DRAW_ID:
			pictureVisible = true;
			InvalidateRect(hWnd, NULL, false);
			break;
		case BUTTON_CLEAR_ID:
			pictureVisible = false;
			InvalidateRect(hWnd, NULL, true);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_DRAWITEM:
	{
		DRAWITEMSTRUCT *draw = (DRAWITEMSTRUCT*)lParam;
		HBRUSH brush;
		HPEN pen;

		switch (draw->CtlID)
		{
			case BUTTON_DRAW_ID:
				{
					HDC hdc = draw->hDC;

					brush = CreateSolidBrush(RGB(255, 255, 255));
					SelectObject(hdc, brush);
					pen = CreatePen(BS_SOLID, 3, RGB(255, 255, 255));
					SelectObject(hdc, pen);
					Rectangle(hdc, 0, 0, 60, 60);

					if (draw->itemAction == ODA_SELECT) {
						brush = CreateSolidBrush(RGB(0, 0, 255));
					}
					else {
						brush = CreateSolidBrush(RGB(0, 255, 0));
					}

					
					pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));

					SelectObject(hdc, brush);
					SelectObject(hdc, pen);

					Ellipse(hdc, 0, 0, 60, 60);
					MoveToEx(hdc, 10, 30, NULL);
					LineTo(hdc, 50, 30);
					MoveToEx(hdc, 30, 10, NULL);
					LineTo(hdc, 30, 50);
				}
				break;
			case BUTTON_CLEAR_ID:
				{
					HDC hdc = draw->hDC;

					brush = CreateSolidBrush(RGB(255, 255, 255));
					SelectObject(hdc, brush);
					pen = CreatePen(BS_SOLID, 3, RGB(255, 255, 255));
					SelectObject(hdc, pen);
					Rectangle(hdc, 0, 0, 60, 60);

					if (draw->itemAction == ODA_SELECT) {
						brush = CreateSolidBrush(RGB(0, 0, 255));
					}
					else {
						brush = CreateSolidBrush(RGB(255, 0, 0));
					}
					pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));

					SelectObject(hdc, brush);
					SelectObject(hdc, pen);

					Ellipse(hdc, 0, 0, 60, 60);
					MoveToEx(hdc, 20, 20, NULL);
					LineTo(hdc, 40, 40);
					MoveToEx(hdc, 40, 20, NULL);
					LineTo(hdc, 20, 40);
				}
				break;
		default:
			break;
		}

	}
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...

		HBRUSH brush;
		HPEN pen;
		if (pictureVisible)
		{
			pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));
			SelectObject(hdc, pen);

			brush = CreateSolidBrush(RGB(11, 0, 166));
			SelectObject(hdc, brush);

			POINT p2[14] = { 470, 250, 560, 250, 560, 190, 750, 190, 750, 280, 470, 280, 470, 250 };
			Polygon(hdc, p2, 14);

			pen = CreatePen(BS_SOLID, 3, RGB(255, 255, 255));
			SelectObject(hdc, pen);
			MoveToEx(hdc, 0, 0, NULL);
			LineTo(hdc, 469, 249);

			brush = CreateSolidBrush(RGB(175, 0, 0));
			SelectObject(hdc, brush);
			pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));
			SelectObject(hdc, pen);

			POINT p[10] = { 70, 90, 190, 10, 210, 10, 330, 90, 70, 90 };
			Polygon(hdc, p, 10);

			pen = CreatePen(BS_SOLID, 3, RGB(255, 255, 255));
			SelectObject(hdc, pen);
			MoveToEx(hdc, 0, 0, NULL);
			LineTo(hdc, 70, 90);

			pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));
			SelectObject(hdc, pen);

			brush = CreateSolidBrush(RGB(50, 50, 50));
			SelectObject(hdc, brush);
			Rectangle(hdc, 70, 90, 330, 270);

			pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));
			SelectObject(hdc, pen);
			brush = CreateSolidBrush(RGB(255, 255, 255));
			SelectObject(hdc, brush);
			Rectangle(hdc, 140, 145, 260, 215);

			MoveToEx(hdc, 200, 145, NULL);
			LineTo(hdc, 200, 215);

			MoveToEx(hdc, 200, 180, NULL);
			LineTo(hdc, 260, 180);

			brush = CreateSolidBrush(RGB(0, 0, 0));
			SelectObject(hdc, brush);
			Ellipse(hdc, 520, 265, 570, 315);
			Ellipse(hdc, 675, 255, 735, 315);

			pen = CreatePen(BS_SOLID, 2, RGB(50, 50, 50));
			SelectObject(hdc, pen);

			Arc(hdc, 400, 100, 500, 150, 500, 125, 450, 100);
			Arc(hdc, 500, 100, 600, 150, 550, 100, 500, 125);

		}
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}